public class Main {

    public static void main(String[] args) {

        //Inner class normal
        InnerClass in = new InnerClass();
        in.hacerAlgo();

        //method local
        claseContenedora outer = new claseContenedora();
        outer.my_Method();

        /// clase inner Anonima
        //
        InnerAnonima inner = new InnerAnonima() {
            @Override
            public void anonimaInner() {
                System.out.println("Ejemplo de anonima inner class");
            }
        };
        inner.anonimaInner();

        //static inner classes
        staticNested.Nested nes=new staticNested.Nested();
        nes.hacerAlgo();

    }
}

