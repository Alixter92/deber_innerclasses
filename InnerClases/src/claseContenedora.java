/**
 * Created by Dario A. Naranjo M. on 24/07/2017.
 */
public class claseContenedora {
    //instacia del metodo de la clase externa
    void my_Method(){
        int num = 23;

        //method-local de inner class
        class MethodInner{
            public void print(){
                System.out.println("Este metodo local inner class "+num);
            }
        }

        //accediendo a la inner class
        MethodInner inner = new MethodInner();
        inner.print();
    }

}


