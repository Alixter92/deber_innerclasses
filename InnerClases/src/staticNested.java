/**
 * Created by Dario A. Naranjo M. on 24/07/2017.
 */

public class staticNested {
    //clase anidada statica de la clase externa
    static class Nested{
        public void hacerAlgo(){
            System.out.println("Ejemplo de una clase static nested class");
        }
    }
}

