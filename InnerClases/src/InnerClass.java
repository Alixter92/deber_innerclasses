/**
 * Created by Dario A. Naranjo M. on 24/07/2017.
 */
public class InnerClass {
    private String x = "Externa2";

    void hacerAlgo(){
        //clase interna como propiedad de la clase exterior
        class Interna{
            public void verExterna(){
                System.out.println("La variable x es: " + x);
            }//cerramos el método de la clase interna
        }//cerramos la clase interna
        Interna i = new Interna();
        i.verExterna();
    }//cerramos el método local de la clase externa
}//cerramos la clase externa

